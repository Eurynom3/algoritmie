# Made by: Louis Bachetti
# Work for: Credit Mutuel Technologies
# Mail: louisbachetti@hotmail.fr
# Description: Method for recursive binarySearch with sorting list (Don't care about bad input)


def find_extreme(listing, extreme, index, mod):
    actual = listing[index]
    if extreme is None or actual*choice > extreme*mod:
        extreme = actual
    if index == len(listing)-1:
        return extreme
    index += 1
    return find_extreme(listing, extreme, index, mod)

listToTest = [int(x) for x in input("Enter list to test \n").split()]
choice = int(input("1=max -1=max"))
print(find_extreme(listToTest,  None, 0, choice))
