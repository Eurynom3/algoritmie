# Made by: Louis Bachetti
# Mail: louisbachetti@hotmail.fr
# Description: Method for recursive binarySearch with sorting list (Don't care about bad input)


def binary_search(number, listing):
    if not listing:
        return False
    index = round(len(listing)/2)
    actual = listing[index]
    if number < actual:
        return binary_search(number, listing[:index])
    elif number > actual:
        return binary_search(number, listing[index:])
    else:
        return True

listToTest = [int(x) for x in input("Enter Int to find \n").split()]
intToFind = int(input("Enter Int to find \n"))
print(binary_search(intToFind, listToTest))



