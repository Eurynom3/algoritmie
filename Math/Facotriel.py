# Made by: Louis Bachetti
# Work for: Credit Mutuel Technologies
# Mail: louisbachetti@hotmail.fr
# Description: Method for recursive binarySearch with sorting list (Don't care about bad input)


def factor_rec(num, total=1):
    if num == 0:
        return total
    else:
        total *= num
        return factor_rec(num-1, total)


def factor_for(num):
    total = 1
    for i in range(num):
        total *= i+1
    return total


x = input("Enter num")
print(factor_for(9))
