def is_palindrome(word):
    length = len(word)-1
    for i in range(length):
        if i >= round(length/2):
            return True
        if word[i] != word[length-i]:
            return False
        continue

print(is_palindrome(""))
